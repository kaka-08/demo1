# 虚拟dom

react在内存中生成维护一个跟真实DOM一样的虚拟DOM树，在改动完组件后，会再生成一个新得DOM，react会把新虚拟DOM跟原虚拟DOM进行比对，找出两个DOM不同的地方diff ，
然后把diff放到队列里面，批量更新diff到真实DOM上。