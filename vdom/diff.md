# 第2节：diff算法

Diff算法的作用是用来计算出 **Virtual DOM** 中被改变的部分，然后针对该部分进行原生DOM操作，而不用重新渲染整个页面。

Diff算法的规则
+ 若节点类型不相同，直接采用替换模式
+ 当节点类型相同时，去看一下属性是否相同，产生一个属性的补丁包
+ 新的DOM节点不存在，也返回一个不存在的补丁包
+ 文本的变化，直接改变文本内容


第一步：虚拟DOM在比较的时候首先比较长度
```
import render from './render';
const zip = (xs,ys) => {
​    const zipped = [];
​    for(let i =0; i<Math.min(xs.length,ys.length); i++){
​      zipped.push([xs[i], ys[i]])
​    }
​    return zipped;
}
```


 第二步：计算差异children

 核心逻辑： 新旧children长度比较，先取最小集合，比较最小集合差异，之后再取新children做新增。

 新旧虚拟DOM的children的最小长度的集合：

```
     e.g. old = {children: [ {tag: 'h1',attrs: { className:'title' }},{tag: 'p',attrs: { className:'context' }} ]}
​     new = {children: [ {tag: 'h1',attrs: { className:'title' }} ]}
​     得出
    [{tag: 'h1',attrs: { className:'title' }},{tag: 'h1',attrs: { className:'title' }}]
    ```

```
const diffChildren = (oldVChildren, newVChildren) => {
​    const childPatches = []; //存储差异
​    for( const[oldVChild, newVChild]  of zip(oldVChildren, newVChildren)){
​        //存储差异操作
​        childPatches.push(diff(oldVChild,newVChild))
​    }
​    //其余的newVChildren
​    const additionalPatches = [];
​    // 注: newVChildren.length > oldVChildren.length, 则一定有结果，反之，不会继续执行
​    for(const additionalVChild of newVChildren.slice(oldVChildren.length)){
​        additionalPatches.push($node => {
          $node.appendChild(render(additionalVChild));
​            return $node;
​        })

​    }
​    //返回一个函数 
​    return $parent => {
​        // 首先，执行差异 更新
​        for( const [patch, child] of zip(childPatches, $parent.childNodes ) ){
​            patch(child)
​        }
​        //执行 新增 如果newVChildren 的长度小于  之前，则不执行
​        for( const patch of additionalPatches){
​            patch($parent)
​        }
​        return $parent;
​    }
}

```
第三步：计算差异属性
```
const diffAttrs = ( oldAttrs, newAttrs ) => {
​    const patches = []; // 更新/删除属性的方法集合
​    //设置新的属性
​    for(const [k,v] of Object.entries(newAttrs) ){
​        patches.push( $node => {
​            $node.setAttribute(k,v);
             return $node
​        });
​    }
​    //删除不存在的属性
​    for(const k in oldAttrs){
​        if(!(k in newAttrs)){
​            patches.push($node => {
​                $node.removeAttribute(k);
​                return $node;
​            })
​        }
​    }
​    //批量更新
​    return $node => {
​        for(const patch of patches){
​            patch($node)
​        }
​    }
}
```

 第四步：计算新旧虚拟DOM的差异

vOldNode  旧的虚拟DOM

vNewNode  新的虚拟DOM

```
const diff = (vOldNode, vNewNode) => {
​    //如果新的虚拟DOM不存在, 则删除对比的DOM节点
​    if(!vNewNode){
​        return $node => { $node.remove(); return null; }
​    }
​    //新的虚拟DOM的tag改变，则完全替换掉
​    if( vOldNode.tagName !== vNewNode.tagName ){
​        return $node => {
​            const $newNode = render(vNewNode);
​            $node.replaceWith($newNode);
​            return $newNode;
​        }
​    }
​    //新旧虚拟DOM为字符串，且不相等
​    if(typeof vOldNode === 'string' || typeof vNewNode === 'string'){
​         if(vOldNode !== vNewNode ){
​             return $node => {
​                const $newNode = render(vNewNode);
​                $node.replaceWith($newNode);
​                return $newNode;
​            }
​        }else{
​            return $node => $node
​        }
​    }
​    //差异属性
​    const patchAttrs =  diffAttrs(vOldNode.attrs, vNewNode.attrs);
​    //差异children
​    const patchChildren =  diffChildren(vOldNode.children, vNewNode.children);
​    return $node => {
​        patchAttrs($node);
​        patchChildren($node);
​        return $node;
​    }
}
export default diff;
```