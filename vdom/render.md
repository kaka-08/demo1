# 第4节：render
函数渲染

 第一步：创建真实的DOM节点

```
const renderElem = ({ tagName, attrs, children })=>{
​    const $el = document.createElement(tagName);
​    // 枚举虚拟DOM的属性，并且讲属性赋值给真实DOM
​    for(const [k,v] of Object.entries(attrs)){
​        $el.setAttribute(k,v)
​    }
​    // 递归虚拟DOM的children属性，最终将所有创建的DOM挂载在父元素上
​    for(const child of children){
​        const $child = render(child);
​        $el.appendChild($child);
​    }
​    return $el;
}
```




第二步：渲染真实的DOM
```
const render = (vNode) =>{
​    if(typeof vNode === 'string' ){
​        return document.createTextNode(vNode);
​    }
​    return renderElem(vNode);
}
export default render;
```
